package com.example.rickandmorty;

import androidx.appcompat.app.AppCompatActivity;


import android.os.Bundle;
import android.content.Intent;
import android.widget.Button;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button characterButton = findViewById(R.id.characters_button);
        Button locationButton = findViewById(R.id.locations_button);
        Button episodeButton = findViewById(R.id.episodes_button);

        characterButton.setOnClickListener(v -> OpenActivity("character"));
        locationButton.setOnClickListener(v -> OpenActivity("location"));
        episodeButton.setOnClickListener(v -> OpenActivity("episode"));
    }
    public void OpenActivity(String value){
        Intent intent = new Intent(this, ItemListActivity.class);
        intent.putExtra("type", value);
        startActivity(intent);
    }
}